mkdir C:\dev\homes\shared\mount\xpm1
mkdir C:\dev\homes\shared\mount\xpm2
mkdir C:\dev\homes\shared\mount\xpm3
mkdir C:\dev\homes\shared\mount\xpd1
mkdir C:\dev\homes\shared\mount\xpd2
mkdir C:\dev\homes\shared\mount\xpd3
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpm1\
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpm2\
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpm3\
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpd1\
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpd2\
copy C:\dev\homes\shared\setenv.sh C:\dev\homes\shared\mount\xpd3\
docker create -v C:/dev/homes/shared/mount/xpm1:/enonic-xp/home -v C:/dev/homes/xp-master-1/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8001:8080 -p 9300:9300 -p 47500:47500 --name xpm1 enonic/xp-app
docker create -v C:/dev/homes/shared/mount/xpm2:/enonic-xp/home -v C:/dev/homes/xp-master-2/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8002:8080 -p 9301:9301 -p 47501:47500 --name xpm2 enonic/xp-app
docker create -v C:/dev/homes/shared/mount/xpm3:/enonic-xp/home -v C:/dev/homes/xp-master-3/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8003:8080 -p 9302:9302 -p 47502:47500 --name xpm3 enonic/xp-app
docker create -v C:/dev/homes/shared/mount/xpd1:/enonic-xp/home -v C:/dev/homes/xp-data-1/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8004:8080 -p 9303:9303 -p 47503:47500 --name xpd1 enonic/xp-app
docker create -v C:/dev/homes/shared/mount/xpd2:/enonic-xp/home -v C:/dev/homes/xp-data-2/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8005:8080 -p 9304:9304 -p 47504:47500 --name xpd2 enonic/xp-app
docker create -v C:/dev/homes/shared/mount/xpd3:/enonic-xp/home -v C:/dev/homes/xp-data-3/config:/enonic-xp/home/config -v C:/dev/homes/shared/config:/enonic-xp/sharedconfig -v C:/dev/homes/shared/blobs:/enonic-xp/blobs -p 8006:8080 -p 9305:9305 -p 47505:47500 --name xpd3 enonic/xp-app

REM call dockerStart.bat