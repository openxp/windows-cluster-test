REM Call this script like below with %~p0 parameter and set SERVICE_NAME variable 
REM call ../getCurrentDirName.bat %~p0
@echo OFF

set mydir="%1"
SET mydir=%mydir:\=;%

for /F "tokens=* delims=;" %%i IN (%mydir%) DO call :LAST_FOLDER %%i
goto :EOF

:LAST_FOLDER
if "%1"=="" (
	set SERVICE_NAME=%LAST%
    goto :RETURN_DIRNAME
)

set LAST=%1
SHIFT

goto :LAST_FOLDER

:RETURN_DIRNAME